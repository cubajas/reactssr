import React from 'react';
import { Route } from 'react-router';
import { Switch } from 'react-router-dom';
import 'normalize.css';
import 'bootstrap-4-grid';

// Pages
import Main from './pages/Main';
import User from './components/User';
import Header from "./components/Header/Header";

class App extends React.Component {
	render() {
		return (
            <div>
                <Header/>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route path="/users" component={User} />
                </Switch>
            </div>
		)
	}
}

export default App;