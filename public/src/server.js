import express  from 'express';
import React    from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import App      from './router';

const app = express();


app.get('*', (req, res) => {
  const context = {};

  const appWithRouter = (
	  <StaticRouter location={req.url} context={context}>
		<App />
	  </StaticRouter>
  );

  if (context.url) {
	res.redirect(context.url);
	return;
  }

  const html = ReactDOMServer.renderToString(appWithRouter);

  res.status(200).send(renderHTML(html));
});


const assetUrl = process.env.NODE_ENV !== 'production' ? 'http://127.0.0.1:8080' : '/';

function renderHTML(componentHTML) {
    return `
    <!DOCTYPE html>
      <html>
      <head>
          <meta charset="utf-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <title>Hello React</title>
          <link rel="stylesheet" href="${assetUrl}/styles.css"/>
      </head>
      <body>
        <div id="react-view">${componentHTML}</div>
        <script type="application/javascript" src="${assetUrl}/bundle.js"></script>
      </body>
    </html>
  `;
}

const PORT = 3001;

app.listen(PORT, () => {
    console.log(`Server listening on: ${PORT}`);
});