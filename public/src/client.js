import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

// Notice that we've organized all of our routes into a separate file.
import App from './router';

class Main extends React.Component {
  render() {
    return (
    	<BrowserRouter>
		  <App />
		</BrowserRouter>
	)
  }
}

const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate
renderMethod(<Main/>, document.getElementById('react-view'));