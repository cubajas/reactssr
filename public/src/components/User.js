import React from 'react';
import { Route } from 'react-router';
import { Switch } from 'react-router-dom';
import UserList from './Users/UserList';
import UserDetail from './Users/UserDetail';



const User = () => (
	<Switch>
	  <Route exact path='/users' component={UserList}/>
	  <Route path='/users/:number' component={UserDetail}/>
	</Switch>
)

export default User;