import React from 'react';
import {Link} from "react-router-dom";
import {Container} from "reactstrap";

const LeftMenu = [
    {
        text: "О бренде",
        link: "/users"
    },
    {
        text: "Полезная информация",
        link: "/users/1"
    },
    {
        text: "Новости",
        link: "/users/2"
    },
    {
        text: "Контакты",
        link: "/users/3"
    }
];

const LeftItems = LeftMenu.map((item, index) =>
    <Link key={ index } to={ item.link }>{ item.text }</Link>
);

const TopLine = () => (
    <div className="top-line">
        <div className="container">
            { LeftItems }
        </div>
    </div>
);

export default TopLine;