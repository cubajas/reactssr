import React from 'react';
import TopLine from './TopLine';

const Header = () => (
    <div>
        <TopLine/>
    </div>
);

export default Header;