const path = require('path');
let ExtractTextPlugin  = require('extract-text-webpack-plugin');
let cssName            = process.env.NODE_ENV === 'production' ? 'styles-[hash].css' : 'styles.css';

module.exports = {
  entry: ['babel-polyfill', './src/client.js'],
  output: {
	path: `${__dirname}/public/assets/`,
	filename: process.env.NODE_ENV === 'production' ? 'bundle-[hash].js' : 'bundle.js'
  },
  resolve: {
	modules: ['node_modules'],
	extensions:         ['*', '.js', '.jsx']
  },
  mode: "development",
  module: {
	rules: [
	  {
		test: /\.css$/,
		use: ExtractTextPlugin.extract({
		  fallback: "style-loader",
		  use: "css-loader"
		})
	  },
	  {
		test: /\.styl/,
		use: ExtractTextPlugin.extract({
		  fallback: "style-loader",
		  use: [
			'css-loader',
			{
			  loader: 'stylus-loader'
			}
		  ]
		})
	  },
	  {
		test: /\.js|jsx$/,
		exclude: ["/node_modules/", "/public/"],
		use: {
		  loader: "babel-loader"
		}
	  }
	]
  },
  plugins: [
	new ExtractTextPlugin(cssName),
  ],
  devServer: {
	headers: { 'Access-Control-Allow-Origin': '*' },
	compress: true,
	disableHostCheck: true,
	host: '127.0.0.1',
	port: 8080,
	index: "index.html",
	publicPath: "/",
	contentBase: path.resolve(__dirname, './public'),
	hot: true
  }
};